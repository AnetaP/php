<?php
/**
 * The main project file.
 *
 */


/**
 * Initialize silex application.
 */
require_once __DIR__.'/../../Projekt/vendor/autoload.php'; 
$app = new Silex\Application();

$app['debug'] = false;


/**
 * Register ServiceProviders.
 */
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(
    new Silex\Provider\TranslationServiceProvider(), array(
    'translator.domains' => array(),
    )
);

$app->register(
    new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../src/views/',
    )
);

/**
 * Connect with database
 */
$app->register(
    new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => '',
        'user'      => '',
        'password'  => '',
        'charset'   => 'utf8',
    ),
    )
);

$app->register(
    new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^.*$',
            'form' => array(
                'login_path' => '/auth/login',
                'check_path' => '/index/login_check',
                'default_target_path'=> '/index/',
                'username_parameter' => 'form[username]',
                'password_parameter' => 'form[password]',
            ),
            'logout'  => true,
            'anonymous' => true,
            'logout' => array('logout_path' => '/auth/logout'),
            'users' => $app->share(
                function() use ($app) {
                    return new User\UserProvider($app);
                }
            ),
        ),
    ),
    'security.access_rules' => array(
        array('^/auth.+$|^/users/add$|^/users/add$|^/posts.*$', 
            'IS_AUTHENTICATED_ANONYMOUSLY'),
        array('^/.+$', 'ROLE_USER'),
        array('^/.+$', 'ROLE_ADMIN')
    
    ),


    'security.role_hierarchy' => array(
        'ROLE_ADMIN' => array('ROLE_USER', 'ROLE_ANONYMUS'),
        'ROLE_USER' => array('ROLE_ANONYMUS'),
    ),
    )
);

/**
 * Mount all Controllers.
 */
$app->register(new Silex\Provider\SessionServiceProvider());

$app->mount('/index/', new Controller\IndexController()); 

$app->mount('/users/', new Controller\UsersController());

$app->mount('/categories/', new Controller\CategoryController());

$app->mount('/auth/', new Controller\AuthController());

$app->mount('/vocabulary/', new Controller\VocabularyController());

$app->mount('/application/', new Controller\AppController());

$app->mount('/posts/', new Controller\PostsController());



$app->get(
    '/', function () use ($app) {
    return $app['twig']->render('news/news.twig');
    }
)->bind('news');




$app->get(
    '/vocabulary/', function () use ($app) {
    return $app['twig']->render('vocabulary/index.twig');
    }
)->bind('vocabulary');

$app->get(
    '/categories/', function () use ($app) {
    return $app['twig']->render('categories/index.twig');
    }
)
    ->bind('categories');

$app->get(
    '/categories/userCategories', function () use ($app) {
    return $app['twig']->render('categories/index.twig');
    }
)
    ->bind('userCategories');    

$app->get(
    '/index/', function () use ($app) {
    return $app['twig']->render('news/news.twig');
    }
)
    ->bind('index');

$app->get(
    '/posts', function () use ($app) {
    return $app['twig']->render('posts/index.twig');
    }
)
    ->bind('posts');

    $app->get(
        '/posts/delete', function () use ($app) {
        return $app['twig']->render('posts/delete.twig');
        }
    )
    ->bind('posts/delete');

$app->get(
    '/posts/list', function () use ($app) {
    return $app['twig']->render('posts/index.twig');
    }
)
    ->bind('posts/list');


$app->get(
    '/contact/', function () use ($app) {
    return $app['twig']->render('contact/index.twig');
    }
)

    ->bind('contact');


$app->get(
    '/about', function () use ($app) {
    return $app['twig']->render('index.twig');
    }
);


$app->get(
    '/auth/login', function () use ($app) {
    return $app['twig']->render('auth/login.twig');
    }
)
    ->bind('login');

$app->get(
    '/users/', function () use ($app) {
    return $app['twig']->render('users/add.twig');
    }
)
    ->bind('users');


$app->get(
    '/users/add', function () use ($app) {
    return $app['twig']->render('users/add.twig');
    }
)
    ->bind('adduser');


$app->get(
    '/users/view', function () use ($app) {
    return $app['twig']->render('users/add.twig');
    }
)
    ->bind('usersview');

$app->get(
    '/application/randomWord', function () use ($app) {
    return $app['twig']->render('application/index.twig');
    }
)
    ->bind('randomWord');

$app->get(
    '/application/guessWord', function () use ($app) {
    return $app['twig']->render('application/guess.twig');
    }
)
    ->bind('guessWord');


$app->get(
    '/application/guessWordCat', function () use ($app) {
    return $app['twig']->render('application/guess.twig');
    }
)
    ->bind('guessWordCat');


/**
* Run the application.
*/
$app->run();
