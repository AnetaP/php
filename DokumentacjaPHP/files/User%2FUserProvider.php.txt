<?php
/**
 * User provider.
 */


/**
 * Define namespace and components.
 */
namespace User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Model\UsersModel;

/**
 * Define user provider
 */
class UserProvider implements UserProviderInterface
{

    /**
     * Application
     *
     * @access protected
     * @var _app  
     */
    protected $_app;

    /**
     * Constructor.
     * 
     * @param application 
     */
    public function __construct($app)
    {
        $this->_app = $app;
    }

    /**
     * Load User data by login
     *
     * @access public
     * @param string $login
     * @return array user
     */
    public function loadUserByUsername($login)
    {
    $userModel = new usersModel($this->_app);
    $user = $userModel->loadUserByLogin($login);
    return new User(
        $user['login'],
        $user['password'], $user['roles'], true, true, true, true
    );
    }

     /**
     * Refresh user to session
     *
     * @access public
     * @param $user
     * @return $user
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.', get_class($user)
                )
            );
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Support class.
     * 
     * @param $class
     * @return $class
     */
    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
}

