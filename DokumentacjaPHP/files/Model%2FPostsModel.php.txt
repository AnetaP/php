<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Model
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Silex\Provider\DoctrineServiceProvider;
 */
namespace Model;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

/**
* Define posts database methods.
*/
class postsModel
{
    /**
    * Database access object.
    *
    * @access protected
    * @var $_db Doctrine\DBAL
    */
    protected $_db;

    /**
    * Class constructor.
    *
    * @access public
    * @param Application $app 
    */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

     /**
     * Get All posts
     *
     * @access public
     * @return array 
     */
    public function getAll ()
    {

        $sql = 'SELECT * FROM posts ORDER BY date DESC';
        return $this->_db->fetchAll($sql);

    }

     /**
     * Get one post by it's ID
     *
     * @access public
     * @param int $idPosts
     * @return array associative array
     */
    public function getPost($idPosts)
    {
        if (($idPosts!= '') && ctype_digit((string)$idPosts)) {
            $sql = 'SELECT * FROM posts WHERE idPosts= ?';
            return $this->_db->fetchAssoc($sql, array((int) $idPosts));
        } else {
            return array();
        }
    }

     /**
     * The list of all posts
     *
     * @access public
     * @param int $idPosts
     * @return array 
     */
    public function indexPost($idPosts)
    {
        if (($idPosts!= '') && ctype_digit((string)$idPosts)) {
            $sql = 'SELECT * FROM posts WHERE idPosts= ?';
            return $this->_db->fetchAll($sql, array((int) $idPosts));
        } else {
            return array();
        }
    }

    /**
     * Add a new post
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function addPost($data, $currentUser)
    {
        $sql = 'INSERT INTO posts (title, content, author) VALUES (?,?,?)';
        $this->_db->executeQuery(
            $sql, array( $data['title'], $data['content'], $currentUser)
        );
    }

    /**
     * Delete Post
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function deletePost($data)
    {
        $sql = 'DELETE FROM posts WHERE idPosts= ?';
        $this->_db->executeQuery($sql, array($data['idPosts']));
    }

    /**
     * Save Post
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function savePost($data)
    {
        if (isset($data['idPosts']) && ctype_digit((string)$data['idPosts'])) {
            $sql = 'UPDATE posts SET title = ?, content = ? WHERE idPosts = ?';
            $this->_db->executeQuery(
                $sql, array(
                $data['title'], $data['content'], $data['idPosts'])
            );
        } else {
            $sql = 'INSERT INTO posts (title, content) VALUES (?,?)';
            $this->_db->executeQuery(
                $sql, array(
                $data['title'], $data['content'])
            );
        }
    }

    /**
     * Check if post exists
     *
     * @access public
     * @param int $idPosts
     * @return boolean
     */
   public function idExist($idPosts)
   {
        if (($idPosts != '') && ctype_digit((string)$idPosts)) {
            $sql = 'SELECT * FROM posts WHERE idPosts= ?';
            if ($this->_db->executeUpdate($sql, array((int) $idPosts)) == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
   }



    /**
     * Count pages
     *
     * @access public
     * @param int $limit
     * @return array 
     */
    public function countPostsPages($limit)
    {
        $pagesCount = 0;
        $sql = 'SELECT COUNT(*) as pages_count FROM posts';
        $result = $this->_db->fetchAssoc($sql);
        if ($result) {
            $pagesCount =  ceil($result['pages_count']/$limit);
        }
        return $pagesCount;
    }

    /**
     * Get page of the post
     * @access public
     * @param int $page
     * @param int $limit
     * @param int $pagesCount
     * @return array 
     */
    public function getPostsPage($page, $limit, $pagesCount)
    {
    if (($page <= 1) || ($page > $pagesCount)) {
        $page = 1;
    }
    $sql = 'SELECT * FROM posts ORDER BY date DESC LIMIT :start, :limit';
    $statement = $this->_db->prepare($sql);
    $statement->bindValue('start', ($page-1)*$limit, \PDO::PARAM_INT);
    $statement->bindValue('limit', $limit, \PDO::PARAM_INT);
    $statement->execute();
    return $statement->fetchAll();
    }

}






