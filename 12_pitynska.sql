-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 13 Wrz 2014, 02:32
-- Wersja serwera: 5.0.51
-- Wersja PHP: 5.3.3-7+squeeze18

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `12_pitynska`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `idAnswers` int(11) NOT NULL auto_increment,
  `right` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `idUsers` int(11) NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `procent` int(11) NOT NULL,
  PRIMARY KEY  (`idAnswers`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=150 ;

--
-- Zrzut danych tabeli `answers`
--

INSERT INTO `answers` (`idAnswers`, `right`, `wrong`, `idUsers`, `date`, `procent`) VALUES
(149, 8, 0, 39, '2014-09-13 02:10:28', 0),
(148, 8, 0, 39, '2014-09-13 02:09:41', 0),
(147, 3, 2, 39, '2014-09-13 02:09:07', 0),
(146, 0, 0, 30, '2014-09-13 01:26:19', 0),
(145, 0, 0, 30, '2014-09-13 01:25:53', 0),
(144, 0, 0, 30, '2014-09-13 01:24:25', 0),
(143, 0, 0, 30, '2014-09-13 01:24:19', 0),
(142, 0, 1, 30, '2014-09-13 01:23:14', 0),
(141, 1, 2, 30, '2014-09-13 01:22:15', 0),
(140, 2, 1, 30, '2014-09-12 23:31:44', 0),
(139, 1, 1, 30, '2014-09-12 23:29:55', 0),
(138, 4, 2, 30, '2014-09-12 23:28:48', 0),
(137, 0, 0, 1, '2014-09-12 23:26:48', 0),
(136, 2, 1, 1, '2014-09-12 23:26:02', 0),
(135, 1, 0, 1, '2014-09-12 23:25:19', 0),
(134, 0, 1, 1, '2014-09-12 23:22:47', 0),
(133, 1, 0, 1, '2014-09-12 23:19:31', 0),
(132, 1, 1, 1, '2014-09-12 23:19:05', 0),
(131, 1, 0, 1, '2014-09-12 23:18:30', 0),
(130, 2, 0, 1, '2014-09-12 23:10:44', 0),
(129, 1, 0, 1, '2014-09-12 23:00:56', 0),
(128, 2, 1, 1, '2014-09-12 23:00:40', 0),
(127, 2, 0, 1, '2014-09-12 22:59:44', 0),
(126, 0, 0, 1, '2014-09-12 22:51:15', 0),
(125, 1, 1, 1, '2014-09-12 22:50:11', 0),
(124, 0, 1, 1, '2014-09-12 19:57:36', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `Categories`
--

CREATE TABLE IF NOT EXISTS `Categories` (
  `idCategories` int(11) NOT NULL auto_increment,
  `nameCategories` char(45) NOT NULL,
  `typeCategories` tinyint(4) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY  (`idCategories`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=118 ;

--
-- Zrzut danych tabeli `Categories`
--

INSERT INTO `Categories` (`idCategories`, `nameCategories`, `typeCategories`, `idUser`) VALUES
(117, 'Kolory', 0, 1),
(49, 'Jedzenie', 0, 1),
(48, 'ZwierzÄ™ta', 0, 1),
(115, 'CzÄ™Å›ci ciaÅ‚a', 0, 2),
(116, 'Sport', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `idPosts` int(11) NOT NULL auto_increment,
  `title` varchar(255) collate utf8_polish_ci NOT NULL,
  `content` text collate utf8_polish_ci NOT NULL,
  `author` varchar(255) collate utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`idPosts`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=20 ;

--
-- Zrzut danych tabeli `posts`
--

INSERT INTO `posts` (`idPosts`, `title`, `content`, `author`, `date`) VALUES
(8, 'Lorem ipsum 1 ', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis accumsan magna. Etiam cursus dolor ut dolor sagittis, sed pharetra arcu porta. Nulla consequat ligula at sem vehicula, eget pretium lectus porta. Sed nec tristique augue. Phasellus quis laoreet felis. Suspendisse vel felis accumsan, volutpat nisl id, mattis justo. Nunc imperdiet ligula imperdiet, scelerisque ligula et, aliquam arcu. Duis venenatis nunc eros. Aliquam dictum fermentum sagittis. Quisque eleifend cursus dapibus. Nam lacinia risus eu eleifend commodo. Nullam pretium feugiat justo id hendrerit. Donec semper metus et est congue sodales. Sed consequat libero ac quam iaculis, et pharetra eros aliquam.\r\n\r\nSed sit amet dui odio. Nulla congue tortor vitae mauris euismod mattis. Sed sed egestas nisi. Proin sit amet volutpat libero. Morbi enim magna, viverra interdum varius sit amet, hendrerit placerat nibh. Cras faucibus scelerisque magna, at fringilla est posuere id. Curabitur sit amet diam ullamcorper, pharetra tortor eget, tristique turpis. Quisque sagittis eros sem, ac tristique ligula adipiscing quis. In pharetra gravida sem, congue sollicitudin odio fermentum eget. Donec a nunc rhoncus, iaculis quam vitae, rhoncus neque. Quisque ac erat congue, congue neque sed, dictum leo. Mauris quis porttitor libero, eu posuere eros. Curabitur vehicula elit lorem, ut pellentesque diam ullamcorper in.\r\n\r\nProin ac metus et mauris vestibulum eleifend non vitae nisl. Suspendisse sit amet ante dolor. Vestibulum id accumsan lorem. Cras pellentesque bibendum sodales. Sed non turpis consequat, dapibus mi ut, ornare lorem. Etiam ut leo dui. Vestibulum mauris risus, luctus et tristique rutrum, hendrerit eu ipsum. Ut adipiscing, tellus vel tincidunt lobortis, dolor sem gravida lorem, vitae rutrum ligula sapien mattis libero. Vivamus viverra mollis lacus, quis hendrerit odio molestie nec. In tincidunt ultricies dapibus. Vestibulum neque ligula, semper et fringilla sit amet, convallis quis mauris. Aenean eu consectetur eros. ', 'admin', '2014-08-20 08:00:00'),
(9, 'Lorem ipsum 2 ', ' Proin ac metus et mauris vestibulum eleifend non vitae nisl. Suspendisse sit amet ante dolor. Vestibulum id accumsan lorem. Cras pellentesque bibendum sodales. Sed non turpis consequat, dapibus mi ut, ornare lorem. Etiam ut leo dui. Vestibulum mauris risus, luctus et tristique rutrum, hendrerit eu ipsum. Ut adipiscing, tellus vel tincidunt lobortis, dolor sem gravida lorem, vitae rutrum ligula sapien mattis libero. Vivamus viverra mollis lacus, quis hendrerit odio molestie nec. In tincidunt ultricies dapibus. Vestibulum neque ligula, semper et fringilla sit amet, convallis quis mauris. Aenean eu consectetur eros.\r\n\r\nAliquam fermentum bibendum nunc, ac tristique lectus rutrum a. Sed aliquet ante a nisl sodales varius. Aliquam at velit a nibh dapibus congue ac sollicitudin ante. Fusce eu feugiat mi, non fringilla massa. Mauris ac lacinia arcu, ac iaculis orci. Vivamus auctor purus eget vehicula auctor. Proin sem dolor, semper quis nulla nec, pellentesque ultricies dolor. Etiam congue leo sed eros dictum eleifend. Mauris eu sagittis odio, eget mattis quam. Fusce facilisis sodales condimentum. Aliquam malesuada tincidunt dolor. In hac habitasse platea dictumst. Duis congue fermentum adipiscing.\r\n\r\nCras fringilla laoreet mi. Vestibulum eu consectetur velit. In mollis auctor luctus. Morbi nec quam ullamcorper, luctus ipsum eget, ullamcorper justo. Integer at luctus tortor, eleifend consectetur felis. In pellentesque libero vel risus congue consequat. Vivamus hendrerit scelerisque lorem, vel molestie purus posuere eget. Etiam et tortor sit amet lacus eleifend pharetra at eget massa. Sed eget tempus arcu. ', 'admin', '2013-08-01 08:29:05'),
(15, 'Lorem ipsum', 'Dolor sit amet, consectetur adipiscing elit. Integer gravida lorem sed placerat elementum. Quisque consectetur et metus ac maximus. Nulla congue, enim ut rutrum accumsan, dolor orci vulputate erat, vitae consequat nisl eros nec quam. Vestibulum feugiat interdum auctor. Duis quam mauris, interdum vitae mattis id, accumsan ac ipsum. Cras quis eros urna. Fusce dapibus porta orci, non porttitor nunc ullamcorper quis. Sed in placerat urna. Maecenas vitae placerat diam, hendrerit accumsan mauris. Praesent dignissim eleifend ornare. Nunc finibus vel dui et dignissim. Aliquam porttitor ante vitae dictum venenatis. Duis pellentesque justo vitae erat pellentesque, ac vestibulum lacus pulvinar. Aliquam blandit pretium sapien, volutpat fermentum eros finibus sit amet. Nulla rhoncus rutrum facilisis. In non pulvinar ligula.\r\n\r\nDuis eget magna non libero maximus euismod. Cras ornare augue sed mattis bibendum. Duis molestie semper tortor non ultricies. In eget bibendum enim, et ornare turpis. Donec vitae tristique ipsum. Nam viverra ligula sapien, non auctor nunc efficitur vehicula. Vivamus venenatis tellus leo, vitae iaculis nisi dictum ut. Etiam dictum orci justo, non gravida sem accumsan id. Proin consectetur sem metus, id molestie orci ullamcorper non. Suspendisse turpis lectus, varius ac nibh vel, dignissim dictum urna. Quisque in ipsum id purus vulputate cursus nec quis erat. Nunc vehicula fermentum tempor. Proin tristique dictum finibus.', 'admin', '2014-09-11 11:27:42'),
(16, 'Mauris eget diam', 'Suspendisse sodales sit amet erat id tincidunt. Vivamus sit amet ex sapien. Mauris eget diam ut massa tincidunt ultrices vel sit amet lacus. Mauris nec pretium massa. Cras egestas, dui non laoreet pulvinar, orci turpis mattis libero, vel semper velit urna at nulla. Pellentesque at cursus odio, in tempus lectus. Donec in sollicitudin arcu, a tristique purus. Nulla eu est leo. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet quam vitae nisl pulvinar ultrices. Nulla rutrum tellus eget ipsum dictum fermentum. Cras eget orci luctus, rutrum metus sed, dapibus lorem.Mauris eget diam ut massa tincidunt ultrices vel sit amet lacus. Mauris nec pretium massa. Cras egestas, dui non laoreet pulvinar, orci turpis mattis libero, vel semper velit urna at nulla. Pellentesque at cursus odio, in tempus lectus. Donec in sollicitudin arcu, a tristique purus. Nulla eu est leo. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet quam vitae nisl pulvinar ultrices. Nulla rutrum tellus eget ipsum dictum fermentum. Cras eget orci luctus, rutrum metus sed, dapibus lorem.', 'admin', '2014-09-11 12:23:19'),
(17, 'Nulla eu est leo.', 'Nulla eu est leo. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing  Mauris eget diam ut massa tincidunt ultrices vel sit amet lacus. Mauris nec pretium massa. Cras egestas, dui non laoreet pulvinar, orci turpis mattis libero, vel semper velit urna at nulla. Pellentesque at cursus odio, in tempus lectus. Donec in sollicitudin arcu, a tristique purus. Nulla eu est leo. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet quam vitae nisl pulvinar ultrices. Nulla rutrum tellus eget ipsum dictum fermentum. Cras eget orci luctus, rutrum metus sed, dapibus lorem.', 'admin', '2014-09-11 12:23:39'),
(18, 'Lorem ipsum dolor sit', 'Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing . Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing .Nulla eu est leo. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing. Nulla eu est leo. Suspendisse non sem gravida, fringilla neque id, euismod quam. Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit', 'admin', '2014-09-11 12:24:14'),
(19, 'Ut sollicitudin dapibus bibendum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut imperdiet diam ac ligula mattis lacinia. Integer sollicitudin dui nec metus blandit accumsan. Integer vehicula erat id ipsum facilisis faucibus. Suspendisse potenti. Ut pellentesque vulputate tellus sit amet placerat. Cras rutrum lacus eu mi eleifend, sit amet luctus sem sodales. Etiam metus dui, pretium ac ullamcorper eu, semper et felis. Duis bibendum eu sem molestie dapibus.\r\n\r\nProin sollicitudin turpis nec est convallis tincidunt. Etiam ut erat eu tortor blandit consectetur nec quis sem. Nam id lacus vel diam dignissim mollis at sed augue. Etiam quis feugiat est. Curabitur fringilla fermentum lacus at cursus. Donec quis mattis sapien, nec sollicitudin nulla. Donec lorem purus, tincidunt eu sagittis in, convallis sit amet turpis. Integer quis mollis nisi.\r\n\r\nUt nec nulla eu neque ultricies dignissim elementum nec magna. Quisque id maximus felis. Integer tincidunt ullamcorper dapibus. Ut ut dolor in ante rutrum luctus. In eget blandit lorem, molestie dignissim tortor. Nam vitae ligula tincidunt, porttitor purus nec, vehicula diam. Curabitur porta eros metus, sit amet ornare ante aliquet sit amet.\r\n\r\nPraesent maximus magna ut magna ultrices consectetur. Ut feugiat mauris ut urna tempor, eget efficitur arcu scelerisque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut nibh enim, commodo efficitur lorem ac, elementum sollicitudin lorem. Nulla ut faucibus ligula. Sed nec nulla mollis eros laoreet fermentum. Praesent pulvinar tellus orci, at rutrum nisi gravida vitae. Vivamus egestas, nisi non mattis molestie, risus nisl vulputate est, ut eleifend massa nisi nec ante. Vestibulum turpis orci, aliquet eu varius eu, eleifend a purus.\r\n\r\nUt sollicitudin dapibus bibendum. Fusce in convallis ante. Sed aliquam velit id sem blandit tristique. Vivamus egestas erat consequat odio elementum tempus. Integer non venenatis mi. Sed lobortis lobortis tellus at suscipit. Integer ac nulla elit.', 'admin', '2014-09-13 01:59:44');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `role` char(32) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `login` char(32) collate utf8_bin NOT NULL,
  `password` char(255) collate utf8_bin NOT NULL,
  `email` char(45) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=40 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`) VALUES
(1, 'admin', 'V42o7RRazJdSCevU6229+3k+hPB3Abq2dsziOxNeFqHpso0u14hB8BSThxFSyYBZQdQ1RBV0Fi+H++stpdv4Cw==', 'admin@admin.pl'),
(3, 'john.doe', 'Lev4gotN3ZSFGNsv6Us2InQ09NjH8sVba2WR76YAsRrn2iL9mLfdXCdOWNOtcX7cFzSQRZ9deafIwyjdd76P9A==', 'john.doe@doe.pl'),
(30, 'Dalek', 'BZTXKtnNlzKQwyVfbQDrCh+g08HeQhJigYYPGSUr+sHBQIzOna7Vsb0QJwrZbtkQRtldlEKFiehmwYQlU1KHtA==', 'dalek@universe123.pl'),
(33, 'renata', 'H3mXOXiqRg7nT+IoR1Uw8Ek2X5hX9hFdZnGDopoedvLfMofSXlM06jKr+anlKFOaG2xqNQe4aHI5tzKNtgEZbA==', 'kareta@onet.eu'),
(39, 'user123', 'BFEQkknI/c+Nd7BaG7AaiyTfUFby/pkMHy3UsYqKqDcmvHoPRX/ame9TnVuOV2GrBH0JK9g4koW+CgTYI9mK+w==', 'user12345@email.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Zrzut danych tabeli `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(3, 3, 2),
(14, 30, 2),
(17, 33, 2),
(23, 39, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `Vocabulary`
--

CREATE TABLE IF NOT EXISTS `Vocabulary` (
  `idVocabulary` int(11) NOT NULL auto_increment,
  `polish` char(45) NOT NULL,
  `spanish` char(45) NOT NULL,
  `comment` char(45) default NULL,
  `type` tinyint(4) NOT NULL,
  `idCategories` int(11) NOT NULL,
  `idUsers` int(11) NOT NULL,
  PRIMARY KEY  (`idVocabulary`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Zrzut danych tabeli `Vocabulary`
--

INSERT INTO `Vocabulary` (`idVocabulary`, `polish`, `spanish`, `comment`, `type`, `idCategories`, `idUsers`) VALUES
(105, 'Å¼Ã³Å‚ty', 'amarillo', NULL, 0, 117, 1),
(104, 'brÄ…zowy', 'marron', NULL, 0, 117, 1),
(103, 'czarny', 'negro', NULL, 0, 117, 1),
(102, 'biaÅ‚y', 'blanco', NULL, 0, 117, 1),
(101, 'czerwony', 'rojo', NULL, 0, 117, 1),
(100, 'zielony', 'verde', NULL, 0, 117, 1),
(99, 'niebieski', 'azul', NULL, 0, 117, 1),
(98, 'wÄ…Å¼', 'serpiente', NULL, 0, 48, 1),
(97, 'koÅ„', 'caballo', NULL, 0, 48, 1),
(96, 'kot', 'gato', 'p.ej el gato negro', 0, 48, 1),
(95, 'pies', 'perro', NULL, 0, 48, 1),
(94, 'maÅ‚pa', 'mono', NULL, 0, 48, 1),
(93, 'pomaraÅ„cza', 'naranja', NULL, 0, 49, 1),
(92, 'banan', 'platano', NULL, 0, 49, 1),
(91, 'jabÅ‚ko', 'manzana', NULL, 0, 49, 1),
(90, 'miÄ™so', 'carne', 'p.ej carne picada', 0, 49, 1),
(89, 'pomidor', 'tomate', 'Pan con tomate', 0, 49, 1),
(88, 'czekolada', 'chocolate', NULL, 0, 49, 1),
(87, 'chleb', 'pan', 'p.ej. Pan con mantequilla', 0, 49, 1),
(86, 'biegaÄ‡', 'correr', NULL, 0, 116, 1),
(85, 'taÅ„czyÄ‡', 'bailar', 'Quiero aprender a bailar salsa.', 0, 116, 1),
(84, 'pÅ‚ywaÄ‡', 'nadar', 'Me gusta mucho nadar en la piscina.', 0, 116, 1);
