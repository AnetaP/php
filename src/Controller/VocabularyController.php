<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Model\CategoryModel;
 * @uses Model\VocabularyModel;
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 */
namespace Controller;
use Model\VocabularyModel;
use Model\CategoryModel;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Define vocabulary methods.
*/
class vocabularyController implements ControllerProviderInterface
{
    /**
    * @access public
    * @param Application $app
    * @return \Silex\ControllerCollection
    */
    public function connect(Application $app)
    {
        $vocabularyController = $app['controllers_factory'];
        $vocabularyController->get('/', array($this, 'index'))
        ->bind('/vocabulary/');
        $vocabularyController
        ->match('/add/{idCategories}', array($this, 'add'))
        ->bind('/vocabulary/add/');
        $vocabularyController
        ->match('/edit/{idVocabulary}', array($this, 'edit'))
        ->bind('/vocabulary/edit/');
        $vocabularyController
        ->match('/delete/{idVocabulary}', array($this, 'delete'))
        ->bind('/vocabulary/delete/');
        $vocabularyController->get('/view', array($this, 'view'))
        ->bind('/vocabulary/view/');
        $vocabularyController
       ->get('/getFromCategory/{idCategories}', array($this, 'getFromCategory'))
        ->bind('/vocabulary/getFromCategory/');
        return  $vocabularyController;
    }

    /**
     * Vocabulary list
     *
     * Displays the list of all vocabulary
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array vocabulary
     */
    public function index (Application $app, Request $request)
    {
        
        $vocabularyModel = new vocabularyModel($app);
        $vocabulary = $vocabularyModel->getAll();
        return $app['twig']->render(
            '/vocabulary/index.twig', array('Vocabulary' => $vocabulary)
        );
    }

     /**
     * Vocabulary list from certain category
     *
     * Displays the list of vocabulary from certain category
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array vocabulary, idCategories, currentUser
     */
    public function getFromCategory (Application $app, Request $request)
    { 

        $vocabularyModel = new vocabularyModel($app);
        $idCategories = (int) $request->get('idCategories', 0);
        $vocabulary = $vocabularyModel->getFromCategory($idCategories);
        
        $categoryModel = new categoryModel($app);
        $category = $categoryModel->getCategory($idCategories);
   
        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);
        $catUser= (int) $category['idUser'];

        return $app['twig']->render(
            '/vocabulary/view.twig', array('Vocabulary' 
                => $vocabulary, 'idCategories' => $idCategories, 'catUser' 
                => $catUser, current => $currentUser)
        );
    }

    /**
     * Add new word
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function add(Application $app, Request $request)
    {
        $usersController = new usersController($app); 
        $idUsers = $usersController-> getIdCurrentUser($app);

        $vocabularyModel = new vocabularyModel($app);
        $idCategories = (int) $request->get('idCategories', 0);

        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)

            ->add(
                'polish', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(
                    array('min' => 2)
                )
                ), 'label' => 'Polski'
                )
            )

            ->add(
                'spanish', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(
                    array('min' => 2)
                )
                ), 'label' => 'Hiszpański'
                )
            )

            ->add(
                'comment', 'textarea', array(
                      'required'    => false, 'constraints' 
                      => array(new Assert\Length(
                          array(
                            'max' => 50
                            )
                      )
                      ), 'label' => 'Komentarz'
                      )
            )

            ->add('Dodaj', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
        
                $vocabularyModel = new vocabularyModel($app);

             $word = $vocabularyModel->getWordByName(
                 $data['polish'], $idCategories
             );
          
            if ($word== null) {
       
            $vocabularyModel->addVocabulary(
                $form->getData(), $idCategories, $idUsers
            );
            $app['session']->getFlashBag()->add(
                'message', array('type' => 'success', 'content'
                 => 'Słówko zostało dodane. Dodaj następne:')
            );
            return $app['twig']->render(
                'vocabulary/add.twig', array(
                    'form' => $form->createView(), 'category' => $idCategories)
            );

            }
 
        $app['session']->getFlashBag()->add(
            'message', array('type' => 'warning', 'content' 
                => 'W tej kategorii juz istenieje takie slowo')
        );

        return $app['twig']->render(
            'vocabulary/add.twig', array(
                'form' => $form->createView(), 'category' => $idCategories)
        );
        }
        return $app['twig']->render(
            'vocabulary/add.twig', array(
                'form' => $form->createView(), 'category' => $idCategories)
        );

    }

    /**
     * Edit word
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Application $app, Request $request)
    {
        

        $vocabularyModel = new vocabularyModel($app);
        $idVocabulary = (int) $request->get('idVocabulary', 0);
        $vocabulary = $vocabularyModel->getVocabulary($idVocabulary);
        
        $cat = $vocabulary['idCategories'];

        $vocUser= (int) $vocabulary['idUsers'];
     

        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);
     

        if (count($vocabulary)) {

            $form = $app['form.factory']->createBuilder('form', $vocabulary)

                ->add(
                    'polish', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 2)
                        )
                        ), 'label' => 'Polski'
                    )
                )

                ->add(
                    'spanish', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 2)
                        )
                        ), 'label' => 'Hiszpański'
                    )
                )

                ->add(
                    'comment', 'textarea', array(
                      'required'    => false, 'constraints' 
                      => array(new Assert\Length(
                          array(
                            'max' => 50
                            )
                      )
                      ), 'label' => 'Komentarz'
                      )
                )

                ->add('Dodaj', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $vocabularyModel = new vocabularyModel($app);
                $vocabularyModel->saveVocabulary($form->getData());
               return $app->redirect('../getFromCategory/'.$cat, 301);
            }

            return $app['twig']->render(
                'vocabulary/edit.twig', array(
                    'form' => $form->createView(), 'vocabulary' 
                    => $vocabulary, 'vocUser' => $vocUser, 'current' 
                    => $currentUser, 'category' => $cat)
            );

        } else {

            return $app->redirect(
                $app['url_generator']->generate('/vocabulary/add/'), 301
            );
        }
    }

    /**
     * Delete word
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function delete(Application $app, Request $request)
    {
        $vocabularyModel = new vocabularyModel($app);

        $idVocabulary = (int) $request->get('idVocabulary');
        $vocabulary = $vocabularyModel->getVocabulary($idVocabulary);
        $cat = $vocabulary['idCategories'];

        $vocUser= (int) $vocabulary['idUsers'];
     

        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);
     


        if (
            ctype_digit(
                (string)$idVocabulary
            ) 
            && $vocabularyModel->idExist($idVocabulary)
            ) {
            $vocabulary = $vocabularyModel->getVocabulary($idVocabulary);
        } else {
            return $app->redirect(
                $app['url_generator']->generate('/categories/'), 301
            );
        }

        if (count($vocabulary)) {

            $form = $app['form.factory']->createBuilder('form', $vocabulary)
                ->add(
                    'idVocabulary', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                ->add(
                    'polish', 'hidden', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 2)
                        )
                    )
                    )
                )
                ->add(
                    'spanish', 'hidden', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 1)
                        )
                    )
                    )
                )
                ->add('Usuń', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $vocabularyModel = new vocabularyModel($app);
                $vocabularyModel->deleteVocabulary($form->getData());
                return $app->redirect('../getFromCategory/'.$cat, 301);
            }

            return $app['twig']->render(
                'vocabulary/delete.twig', array(
                    'form' => $form->createView(), 'Vocabulary'
                     => $vocabulary, 'vocUser' => $vocUser, 'current'
                     => $currentUser, 'category' => $cat)
            );
        } else {
           return $app->redirect('../getFromCategory/'.$cat, 301);
        }
    }

}

