<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 * @uses Model\usersModel;
 */
namespace Controller;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\usersModel;

 /**
 * Define authorization methods.
 */
class AuthController implements ControllerProviderInterface
{
   
    /**
    * @access public
    * @param Application $app
    * @return \Silex\ControllerCollection
    */
    public function connect(Application $app)
    {
        $authController = $app['controllers_factory'];
        $authController->match('/login', array($this, 'login'))->
        bind('/auth/login');
        $authController->match('/logout', array($this, 'logout'))->
        bind('/auth/logout');
        return $authController;
    }

    /**
     * Login
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function login(Application $app, Request $request)
    {
        $data = array();

        $form = $app['form.factory']->createBuilder('form')
            ->add(
                'username', 'text', array(
                'label'=>'Login', 'data'
                    => $app['session']->get('_security.last_username')
                )
            )
            ->add(
                'password', 'password', array('label' => 'Hasło')
            )
            ->add('Zaloguj się', 'submit')
            ->getForm();

        return $app['twig']->render(
            'auth/login.twig', array(
                'form' => $form->createView(), 'error' 
                => $app['security.last_error']($request)
                )
        );
    }



    /**
     * Logout
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Application $app, Request $request)
    {
        $app['session']->clear();
        return $app['twig']->render('auth/logout.twig');
    }

}