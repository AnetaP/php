<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Model\PostsModel
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 */
namespace Controller;
use Model\PostsModel;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


/**
* Define posts methods.
*/
class postsController implements ControllerProviderInterface
{
    /**
    * @access public
    * @param Application $app
    * @return \Silex\ControllerCollection
    */
    public function connect (Application $app)
    {
          $postsController= $app['controllers_factory'];
          $postsController->match('/delete/{idPosts}', array($this, 'delete'))
          ->bind('/posts/delete/');
          $postsController->match('/edit/{idPosts}', array($this, 'edit'))
          ->bind('/posts/edit/');
          $postsController->get('/getPost/{idPosts}', array($this, 'getPost'))
          ->bind('/posts/getPost/');
          $postsController->match('/list', array($this, 'getList'))
          ->bind('/posts/list/');
          $postsController->match('/add', array($this, 'addPost'))
          ->bind('/posts/add/');
          $postsController->get('/{page}', array($this, 'index'))
          ->value('page', 1)->bind('/posts/');
         
          
        return $postsController;
    }

     /**
     * Gets the list of the posts
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getList (Application $app, Request $request)
    {
        $postsModel = new postsModel($app);
        $posts = $postsModel->getAll();
        return $app['twig']->render(
            '/posts/list.twig', array('posts' => $posts)
        );
    }

     /**
     * Delete post
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Application $app, Request $request)
    {
        $postsModel = new postsModel($app);
        $idPosts = (int) $request->get('idPosts');
        if (ctype_digit((string)$idPosts) && $postsModel->idExist($idPosts)) {
            $post = $postsModel->getPost($idPosts);
             
        } else {
       
            return $app->redirect(
                $app['url_generator']->generate('/posts/'), 301
            );
        }

        if (count($post)) {

            $form = $app['form.factory']->createBuilder('form', $post)
                ->add(
                    'idPosts', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                
                ->add('Usuń', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $postsModel = new postsModel($app);
                $postsModel->deletePost($form->getData());
                $app['session']->getFlashBag()->add(
                    'message', 'Usunięto post!'
                );
                return $app->redirect(
                    $app['url_generator']->generate('/posts/'), 301
                );
            }
           
            return $app['twig']->render(
                'posts/delete.twig', array(
                    'form' => $form->createView(), 'Post' => $post)
            );

        } else {
            return $app->redirect(
                $app['url_generator']->generate('/posts/'), 301
            );
        }
    }

     /**
     * Edit post
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Application $app, Request $request)
    {
        $postsModel = new postsModel($app);
        $idPosts = (int) $request->get('idPosts', 0);

        $posts = $postsModel->getPost($idPosts);

        if (count($posts)) {

            $form = $app['form.factory']->createBuilder('form', $posts)
                ->add(
                    'idPosts', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                ->add(
                    'title', 'text', array(
                    'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' => 2)
                    )), 'label' => 
                    'Tytuł'
                    )
                )
                ->add(
                    'content', 'textarea', array(
                    'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' =>50)
                    )
                    ), 'attr' => array(
                        'style' => 'width: 50%; height: 300px'), 'label' => 
                        'Treść'
                    )
                )
                ->add('Zapisz', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $postsModel = new postsModel($app);
                $postsModel->savePost($form->getData());
                return $app->redirect(
                    $app['url_generator']->generate('/posts/'), 301
                );
            }

            return $app['twig']->render(
                'posts/edit.twig', array(
                    'form' => $form->createView(), 'posts' => $posts)
            );

        } else {

           return $app->redirect(
               $app['url_generator']->generate('/posts/add/'), 301
           );
        }

    }
       

    /**
     * Display all post
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array posts, paginator
     */
    public function index(Application $app, Request $request)
    {
        $pageLimit = 3;
        $page = (int) $request->get('page', 1);
        $postsModel = new postsModel($app);
        $pagesCount = $postsModel->countPostsPages($pageLimit);
       
        if (($page < 1) || ($page > $pagesCount)) {
            $page = 1;
        }
        $posts = $postsModel->getPostsPage($page, $pageLimit, $pagesCount);
        $paginator = array('page' => $page, 'pagesCount' => $pagesCount);

        return $app['twig']->render(
            '/posts/index.twig', array(
            'posts' => $posts, 'paginator' => $paginator)
        );
    }

    /**
     * Get one post by it's ID
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array posts
     */
    public function getPost (Application $app, Request $request)
    {
        $postModel = new postsModel($app);
        $idPosts = (int) $request->get('idPosts', 0);
        $posts= $postModel->indexPost($idPosts);
       
        return $app['twig']->render(
            '/posts/view.twig', array('posts' => $posts)
        );
    }
 
     /**
     * Add new post
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addPost(Application $app, Request $request)
    {
        $usersController = new usersController($app); 
        $currentUser = $usersController-> getNameCurrentUser($app);
       
        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)

            ->add(
                'title', 'text', array(
                'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' => 2)
                    )), 'label' => 
                    'Tytuł'
                )
            )
            ->add(
                'content', 'textarea', array(
                'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' =>50)
                    )
                ), 'attr' => array(
                        'style' => 'width: 50%; height: 300px'), 'label' => 
                        'Treść'
                )
            )

            ->add('Dodaj', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $postsModel = new postsModel($app);
            $postsModel->addPost($data, $currentUser);
            return $app->redirect(
                $app['url_generator']->generate('/posts/'), 301
            );
        }

        return $app['twig']->render(
            'posts/add.twig', array('form' => $form->createView())
        );
    }

}

