<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Model
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Silex\Provider\DoctrineServiceProvider;
 */
namespace Model;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

/**
* Define application database methods.
*/
class AppModel
{
     /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app 
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Get's the list of vocabulary ID's
     *
     * @access public
     * @return array
     */
    public function getID()
    {
        $sql = 'SELECT idVocabulary FROM Vocabulary';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Get's random vocabulary ID
     *
     * @access public
     * @return array associative array
     */
    public function getRandomID()
    {

      $sql = "SELECT * FROM `Vocabulary` WHERE 
       idUsers = 1 ORDER BY RAND() LIMIT 1;";
 


        return $this->_db->fetchAssoc($sql);
    }

    /**
     * Get's random word
     *
     * @param string $condition
     * @access public
     * @return array associative array 
     */
    public function getRandomWord($condition)
    {

         $sql = "SELECT * FROM `Vocabulary` WHERE 
         idVocabulary NOT IN (".$condition.") AND 
         idUsers = 1 ORDER BY RAND() LIMIT 1;";
 
        return $this->_db->fetchAssoc($sql);
    }

    /**
     * Get's random word from a certain category
     *
     * @param string $condition 
     * @param int $id
     * @access public
     * @return array associative array
     */
    public function getRandomWordCat($condition, $id)
    {

     $sql = "SELECT * FROM `Vocabulary` WHERE 
     idVocabulary NOT IN (".$condition.") AND idCategories = (".$id.")  
     ORDER BY RAND() LIMIT 1;";
 
        return $this->_db->fetchAssoc($sql);
    }

    
     /**
     * Add final result of the test
     *
     * @access public
     * @param string $right
     * @param string $wrong
     * @param int $idUsers
     * @return Void
     */
    public function addFinalResult($right, $wrong, $idUsers)
    {

        $sql = "INSERT INTO `answers` (`right`, `wrong`, `idUsers`) 
        VALUES (?,?,?);";
        $this->_db->executeQuery($sql, array($right, $wrong, $idUsers));
    }

     /**
     * Gets results of a certain user
     *
     * @access public
     * @param int $idUsers
     * @return array 
     */
    public function getResults($idUsers)
    {
        $sql = "SELECT * FROM `answers` WHERE 
        idUsers = (".$idUsers.") ORDER BY DATE DESC;";
        return $this->_db->fetchAll($sql);
    }


}





